/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

/**
 *
 * @author Hugo
 */
public class Group {

    private String name;
    private Folder root;
    private User manager;
    
    //Displaying
    private Folder currentFolder;
    
    public Group(String name) {
        this.name = name;
        this.root = new Folder("root", null);
        this.currentFolder = this.root;
    }
    
    public Group(String name, User manager) {
        this.name = name;
        this.root = new Folder("root", null);
        this.manager = manager;
        this.currentFolder = this.root;
    }
    
    public Group(String name, User manager, Group templateGroup) {
        this.name = name;
        this.root = templateGroup.getTemplate();
        this.manager = manager;
        this.currentFolder = this.root;
    }

    boolean isNamed(String name) {
        if (this.name.equals(name)) {
            return true;
        }
        return false;
    }

    public Folder getRoot() {
        return this.root;
    }
    
    public String displayName() {
        return this.name;
    }
    
    @Override
    public String toString() {
        return
            "Group:"+this.name+"\n\n"+
            this.root.listChildren();
    }
    
    public boolean checkManager(User user) {
        return user == this.manager;
    }
    
    public Folder getTemplate() {
        return this.root.cloneStructure(null);
    }
    
    //Displaying
    public String getCurrentFolder() {
        return this.name + ":" + this.currentFolder.getAbsoluteURI();
    }
    
    public String displayCurrentFolderContent() {
        return this.currentFolder.listChildren();
    }
    
    public void goTo(String uri) throws Exception {
        this.currentFolder = this.currentFolder.goTo(uri);
    }
    
    public void setCategory(String name, String targetUri) throws Exception {
        this.currentFolder.getByURI(targetUri).setCategory(name);
    }
    
    public void applyRelation(String relName, String uriFrom, String uriTo) throws Exception {
        AbstractObject aoFrom;
        AbstractObject aoTo;
        try{
            aoFrom = this.currentFolder.getByURI(uriFrom);
        } catch (Exception e) {
            throw new Exception(e.getMessage() + "uri from");
        }
        try{
            aoTo = this.currentFolder.getByURI(uriTo);
        } catch (Exception e) {
            throw new Exception(e.getMessage() + "uri to");
        }
        try{
            Ent.Instance().getRelationFactory().applyRelation(relName, aoFrom, aoTo);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public String listRelations() {
        return this.currentFolder.listRelations();
    }
    
    public String listRelations(String uri) throws Exception {
        return this.currentFolder.getByURI(uri).listRelations();
    }
    
    public void createDocument(String name) throws Exception {
        this.currentFolder.addDocument(name);
    }
    public void createService(String name) throws Exception {
        this.currentFolder.addService(name);
    }
    public void createFolder(String name) throws Exception {
        this.currentFolder.addSubFolderAndChain(name);
    }
}
