/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class FolderTest {
    
    
    public FolderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testFolderCanBeAdded() throws Exception {
        Folder folder = new Folder("Folder", null);
        folder.addSubFolderAndGetChild("TestSubFolder");
        assertNotNull((folder.getChildByName("TestSubFolder")));
    }
    
    @Test
    public void testAddSubFolderMethodAddsFolders() throws Exception {
        Folder folder = new Folder("Folder", null);
        folder.addSubFolderAndGetChild("TestSubFolder");
        assertTrue((folder.getChildByName("TestSubFolder") instanceof Folder));
    }
    
    @Test
    public void testServiceCanBeAdded() throws Exception {
        Folder folder = new Folder("Folder", null);
        folder.addService("TestSubService");
        assertNotNull((folder.getChildByName("TestSubService")));
    }
    
    @Test
    public void testAddServiceMethodAddsServices() throws Exception {
        Folder folder = new Folder("Folder", null);
        folder.addService("TestSubService");
        assertTrue((folder.getChildByName("TestSubService") instanceof Service));
    }
    
    @Test
    public void testDocumentCanBeAdded() throws Exception {
        Folder folder = new Folder("Folder", null);
        folder.addDocument("TestSubDocument");
        assertNotNull((folder.getChildByName("TestSubDocument")));
    }
    
    @Test
    public void testAddDocumentMethodAddsDocuments() throws Exception {
        Folder folder = new Folder("Folder", null);
        folder.addDocument("TestSubDocument");
            assertTrue((folder.getChildByName("TestSubDocument") instanceof Document));
    }
    
    @Test
    public void testChildsAreWellDisplayed() throws Exception {
        Folder folder = new Folder("Folder", null);
        assertEquals(folder.listChildren(), "");
        folder.addDocument("TestSubDocument");
        folder.addSubFolderAndGetChild("TestSubFolder1");
        folder.addService("TestSubService");
        folder.addSubFolderAndGetChild("TestSubFolder2");
        String goodChildList = 
                "D:TestSubDocument\n"+
                "F:TestSubFolder1\n"+
                "S:TestSubService\n"+
                "F:TestSubFolder2\n";
        assertEquals(folder.listChildren(), goodChildList);
    }
    
    @Test
    public void testParentIsWellDisplayed() throws Exception {
        Folder folder = new Folder("Folder", null);
        folder.addSubFolderAndGetChild("TestSubFolder");
        assertEquals(((Folder)folder.getChildByName("TestSubFolder")).displayParent(), "P:Folder\n");
    }
    
    @Test
    public void testGetByURI() throws Exception {
        Folder folder1 = new Folder("Folder1", null);
        folder1.addSubFolderAndGetChild("Folder11");
        folder1.addSubFolderAndGetChild("Folder12");
        Folder folder11 = (Folder)folder1.getChildByName("Folder11");
        folder11.addSubFolderAndGetChild("Folder111");
        folder11.addSubFolderAndGetChild("Folder112");
        folder11.addSubFolderAndGetChild("Folder113");
        Folder folder12 = (Folder)folder1.getChildByName("Folder12");
        folder12.addSubFolderAndGetChild("Folder121");
        
        //From folder121...
        Folder folder121 = (Folder)folder12.getChildByName("Folder121");
        //I want to get the folder 113
        Folder folder113 = (Folder)folder11.getChildByName("Folder113");
        
        assertEquals(folder113, folder121.getByURI("../../Folder11/Folder113"));
        
    }
    
    @Test
    public void testClone() throws Exception {
        Folder folder1 = new Folder("Folder1", null);
        folder1
            .addSubFolderAndGetChild("Folder11")
                .addSubFolderAndChain("Folder111")
                .addSubFolderAndChain("Folder112")
                .addSubFolderAndChain("Folder113")
            .goTo("..")
            .addSubFolderAndGetChild("Folder12")
                .addSubFolderAndGetChild("Folder121");
        
        Folder clone = folder1.cloneStructure(null);
        
        assertFalse(clone == folder1);
        assertEquals(folder1.getName(), clone.getName());
        //Must not throw exception
        assertFalse(folder1.getByURI("Folder11/Folder111") == clone.getByURI("Folder11/Folder111"));
        assertFalse(folder1.getByURI("Folder11/Folder112") == clone.getByURI("Folder11/Folder112"));
        assertFalse(folder1.getByURI("Folder11/Folder113") == clone.getByURI("Folder11/Folder113"));
        assertFalse(folder1.getByURI("Folder12/Folder121") == clone.getByURI("Folder12/Folder121"));
        
        assertEquals(clone.getByURI("Folder11/Folder111").getParent(),clone.getByURI("Folder11"));
    }
}