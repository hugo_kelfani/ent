/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

/**
 *
 * @author Yohann
 */
class Relation {
    private String name;
    private AbstractObject from;
    private AbstractObject to;
    
    public Relation(String name, AbstractObject from, AbstractObject to) {
        this.name = name;
        this.from = from;
        this.to = to;
    }
    
    public AbstractObject getFrom() {
        return this.from;
    }
    
    public AbstractObject getTo() {
        return this.to;
    }
    
    public String getName() {
        return this.name;
    }
}
