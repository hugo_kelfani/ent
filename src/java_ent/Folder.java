/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import java.util.ArrayList;

/**
 *
 * @author Hugo
 */
public class Folder extends AbstractObject {

    private ArrayList<AbstractObject> children;

    public Folder(String folder, Folder parent) {
        super(folder, parent);
        this.children = new ArrayList<AbstractObject>();
    }

    public Folder addSubFolderAndGetChild(String name) throws Exception {
        if(!this.alereadyContainsChildNamed(name)) {
            Folder newChild = new Folder(name, this);
            this.children.add(newChild);
            return newChild;
        }
        throw new Exception("Folder already exists in this repository");
    }
    
    public Folder addSubFolderAndChain(String name) throws Exception {
        if(!this.alereadyContainsChildNamed(name)) {
            Folder newChild = new Folder(name, this);
            this.children.add(newChild);
            return this;
        }
        throw new Exception("Folder already exists in this repository");
    }

    public Folder addDocument(String name) {
        if(!this.alereadyContainsChildNamed(name)) {
            this.children.add(new Document(name,this));
        }
        return this;
    }

    public Folder addService(String name) {
        if(!this.alereadyContainsChildNamed(name)) {
            this.children.add(new Service(name,this));
        }
        return this;
    }

    public String displayParent() {
        return this.getParentName()+"\n";
    }

    public String listChildren() {
        String childrenList = "";
        for(AbstractObject child : this.children) {
            childrenList += child.getName()+"\n";
        }
        return childrenList;
    }

    public AbstractObject getChildByName(String name) throws Exception {
        for(AbstractObject ao : this.children) {
            if(ao.isNamed(name)) {
                return ao;
            }
        }
        throw new Exception("File not found");
    }

    @Override
    public Boolean isNamed(String name) {
        return this.name.equals(name);
    }
    
    public String getName() {
        return "F:"+this.name;
    }
    
    public String getNameAsParent() {
        return "P:"+this.name;
    }
    

    public AbstractObject getByURI(String uri) throws Exception {
        String head = this.head(uri);
        String rest = this.rest(uri);
        
        if(rest == null) {
            if(head.equals("..")) {
                return this.parent;
            }
            return this.getChildByName(head);
        }
        
        if(head.equals("..")) {
            if(this.parent == null) {
                return this.getByURI(rest);
            } else {
                return this.parent.getByURI(rest);
            }
        }
        
        if(this.getChildByName(head) instanceof Folder) {
            return ((Folder)this.getChildByName(head)).getByURI(rest);
        }
        
        throw new Exception("Invalid URI");
    }
    
    public Folder goTo(String uri) throws Exception {
        AbstractObject target = this.getByURI(uri);
        if(target.isFolder()) {
            return (Folder) target;
        }
        
        throw new Exception("Target must be a folder");
    }
    
    private boolean alereadyContainsChildNamed(String name) {
        for(AbstractObject ao : this.children) {
            if(ao.isNamed(name)) {
                return true;
            }
        }
        return false;
    }
    
    private String getParentName() {
        return this.parent.getNameAsParent();
    }
    
    private String head(String uri) {
        return (uri.split("/"))[0];
    }
    
    private String rest(String uri) {
        String[] splittedURI = (uri.split("/"));
        
        if(splittedURI.length <= 1) {
            return null;
        }
        
        String returned = "";
        for(int i = 1; i < splittedURI.length; ++i) {
            returned += splittedURI[i];
            if(i < splittedURI.length - 1) {
                returned += "/";
            }
        }
        return returned;
    }

    @Override
    protected boolean isFolder() {
        return true;
    }
    
    public Folder cloneStructure(Folder parent) {
        Folder clone = new Folder(this.name,parent);
        for(AbstractObject ao : this.children) {
            if(ao.isFolder()) {
                clone.children.add(((Folder)ao).cloneStructure(clone));
            }
        }
        return clone;
    }
}
