/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import java.util.ArrayList;

/**
 *
 * @author Hugo
 */
public abstract class AbstractObject {
    protected Folder parent;
    protected String name;
    
    public Category category;
    public ArrayList<Relation> outgoingRelations;
    public ArrayList<Relation> incomingRelations;
    
    public AbstractObject(String name, Folder parent) {
        this.name              = name;
        this.parent            = parent;
        this.incomingRelations = new ArrayList<>();
        this.outgoingRelations = new ArrayList<>();
    }
    public abstract Boolean isNamed(String name);
    public abstract String getName();
    public void setCategory(String name) {
        this.category = Ent.Instance().getCategoryByName(name);
    }
    
    public Category getCategory() {
        return this.category;
    }
    
    public int countOutgoingRelations() {
        return this.outgoingRelations.size();
    }
    
    public int countIncomingRelations() {
        return this.incomingRelations.size();
    }

    public void addOutgoingRelation(Relation r) {
        this.outgoingRelations.add(r);
    }

    public void addIncomingRelation(Relation r) {
        this.incomingRelations.add(r);
    }
    
    public Folder getParent() throws Exception {
        if(this.parent == null) {
            if(this.isFolder()) {
                return (Folder) this;
            }
            throw new Exception("Root must be a folder.");
        }
        return this.parent;
    }

    abstract protected boolean isFolder();

    public String getAbsoluteURI() {
        if(this.parent == null) {
            return this.name;
        }   
        
        return this.parent.getAbsoluteURI() + "/" + this.name;
    }
    
    public String listRelations() {
        String returned = "";
        
        for(Relation r : this.outgoingRelations) {
            returned += r.getName()+" > "+r.getTo().getAbsoluteURI()+"\n";
        }
        
        for(Relation r : this.incomingRelations) {
            returned += r.getName()+" < "+r.getFrom().getAbsoluteURI()+"\n";
        }
        
        return returned;
        
    }
}
