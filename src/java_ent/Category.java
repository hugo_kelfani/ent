/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

/**
 *
 * @author Yohann
 */
class Category {
    private String name;
    
    public Category(String catName) {
        this.name = catName;
    }
    
    @Override
    public boolean equals(Object cat) {
        if(!(cat instanceof Category)) {
            return false;
        }
        
        String name1 = ((Category)cat).name.toLowerCase();
        String name2 = this.name.toLowerCase();
        return name1.equals(name2);
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
