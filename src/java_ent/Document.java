/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

/**
 *
 * @author Hugo
 */
public class Document extends AbstractObject {

    public Document(String name, Folder parent) {
        super(name,parent);
    }
    
    @Override
    public Boolean isNamed(String name) {
        return this.name.equals(name);
    }

    @Override
    public String getName() {
        return "D:"+this.name;
    }

    @Override
    protected boolean isFolder() {
        return false;
    }
}
