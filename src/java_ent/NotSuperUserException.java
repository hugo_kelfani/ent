/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

/**
 *
 * @author Yohann
 */
class NotSuperUserException extends Exception {

    public NotSuperUserException() {
        super("Not enough acces. Must be a super user");
    }
}
