/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class GroupTest {

    private Group g1;
    private Folder root;

    public GroupTest() {
        g1 = new Group("Group1");
        root = this.g1.getRoot();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
    }

    @Test
    public void testNewGroupHasRoot() {
        assertTrue((this.g1.getRoot() instanceof Folder));
    }
    
//    @Test
//    public void testGroupStructureIsNavigableDown() {
//        Group g1 = new Group("Group1");
//        Folder root = g1.getRoot();
//        root.addSubFolder("TestSubFolder");
//        Folder subFolder = root.getChildByName("TestSubFolder");
//        assertTrue((root.getChildByName("TestSubFolder") instanceof Folder));
//    }

    @Test
    public void testGroupNameIsWellDisplayed() {
        assertEquals(this.g1.displayName(), "Group1");
    }

    @Test
    public void testContentIsWellDisplayed() throws Exception {
        root.addDocument("Doc1");
        root.addSubFolderAndGetChild("Fol1");
        root.addService("Ser1");
        root.addSubFolderAndGetChild("Fol2");
        root.addDocument("Doc2");
        root.addSubFolderAndGetChild("Fol3");
        root.addService("Ser2");
        root.addDocument("Doc3");
        root.addService("Ser3");
        String goodStringDisplayed =
                "Group:Group1\n"
                + "\n"
                + "D:Doc1\n"
                + "F:Fol1\n"
                + "S:Ser1\n"
                + "F:Fol2\n"
                + "D:Doc2\n"
                + "F:Fol3\n"
                + "S:Ser2\n"
                + "D:Doc3\n"
                + "S:Ser3\n";
        assertEquals(goodStringDisplayed,this.g1.toString());
    }
    
    @Test
    public void testGroupStructureIsNavigableDown() throws Exception {
        Group g1 = new Group("Group1");
        Folder root = g1.getRoot();
        root.addSubFolderAndGetChild("TestSubFolder");
        Folder subFolder = (Folder)root.getChildByName("TestSubFolder");
        assertTrue((root.getChildByName("TestSubFolder") instanceof Folder));
    }
}