/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ViewController;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java_ent.User;
import java_ent.Ent;

/**
 *
 * @author Pret-Capes
 */
public class ViewController {

    //utils
    private static Scanner scanner = new Scanner(System.in);
    private static boolean exit = false;
    private static String context = ">";

    //Model entries
    private static User currentUser = null;
    private static Ent ent = Ent.Instance();

    public static void main(String[] args) {

        echo("Welcome to java ent !");
        echo("=++++++++++++++++++++++++=");
        echo("By Y. Berthon & H. Kelfani");
        echo("=++++++++++++++++++++++++=");
        jump();

        while (!exit) {
            System.out.print(context);
            if (currentUser == null) {
                unConnectedController(scan());
            } else {
                connectedController(scan());
            }
            refreshContext();
        }
    }

    private static void unConnectedController(String cmd) {
        String[] splittedCmd = cmd.split(" ");
        try{
            switch (splittedCmd[0]) {
                case "newSU":
                    User su = new User(splittedCmd[1]);
                    ent.addUser(su);
                    ent.addSuperUser(su);
                    break;
                case "newU":
                    User u = new User(splittedCmd[1]);
                    ent.addUser(u);
                    break;
                case "listUsers":
                    echo(ent.listUsers());
                    break;
                case "connect":
                    currentUser = ent.getUserByName(splittedCmd[1]);
                    break;
                case "exit":
                    exit = true;
                    break;
                default:
                    echo("Commands :");
                    echo("(cmd [arg1] [arg2] ...)");
                    jump();
                    echo("newSU [name]");
                    echo("  Try to create a new super user with the name [name] ");
                    jump();
                    echo("newU [name]");
                    echo("  Try to create a new user with the name [name] ");
                    jump();
                    echo("listUsers");
                    echo("  List the users registered in the ENT, * for super user ");
                    jump();
                    echo("connect [name]");
                    echo("  Try to connect with the user named [name] ");
                    jump();
                    echo("exit");
                    echo("  ByeBye :-) ");
                    jump();
            }
        } catch (Exception e) {
            invalidCommand(e);
        }
    }

    private static void connectedController(String cmd2Split) {
        String[] cmd = cmd2Split.split(" ");
        try {
            switch (cmd[0]) {
                case "listUsers":
                    echo(ent.listUsers());
                    break;
                case "listGroups":
                    echo(currentUser.listGroups());
                    break;
                case "createGroup":
                    if(cmd.length > 2) {
                        currentUser.createGroup(cmd[1], cmd[2]);
                    } else {
                        currentUser.createGroup(cmd[1]);
                    }
                    break;
                case "addUserToGroup":
                    currentUser.addUserInGroup(cmd[1], cmd[2]);
                    break;
                case "openGroup":
                    currentUser.openGroup(cmd[1]);
                    break;
                case "cd":
                    currentUser.goTo(cmd[1]);
                    break;
                case "ls":
                    echo(currentUser.displayContent());
                    break;
                case "mkdir":
                    currentUser.createFolder(cmd[1]);
                    break;
                case "mkser":
                    currentUser.createService(cmd[1]);
                    break;
                case "mkdoc":
                    currentUser.createDocument(cmd[1]);
                    break;
                case "createRule":
                    currentUser.addRule(cmd[1], cmd[2], cmd[3]);
                    break;
                case "listRules":
                    echo(ent.listRules());
                    break;
                case "setCat":
                    currentUser.setCategory(cmd[1], cmd[2]);
                    break;
                case "applyRel":
                    currentUser.applyRelation(cmd[1], cmd[2], cmd[3]);
                    break;
                case "listRel":
                    if(cmd.length > 1) {
                        echo(currentUser.displayRelations(cmd[1]));
                    } else {
                        echo(currentUser.displayRelations());
                    }
                    break;
                case "disconnect":
                    currentUser = null;
                    break;
                case "exit":
                    exit = true;
                    break;
                default:
                    echo("Commands :");
                    echo("(cmd [arg1] [arg2] ...)");
                    jump();
                    echo("listUsers");
                    echo("  List registered users, * for super user");
                    jump();
                    echo("listGroups");
                    echo("  List groups for current user, * for managed groups");
                    jump();
                    echo("createGroup [name]");
                    echo("  Create a group with [name]");
                    jump();
                    echo("createGroup [name] [templateName]");
                    echo("  Create a groupe with [name] based on group with [templateName]");
                    jump();
                    echo("addUserToGroup [userName] [groupName]");
                    echo("  Add user [userName] to group [groupName]");
                    jump();
                    echo("openGroup [groupName]");
                    echo("  Opens group [groupName]");
                    jump();
                    echo("cd [uri]");
                    echo("  Go to [uri] in the current group");
                    jump();
                    echo("ls");
                    echo("  Display the content of the current repository in the current group");
                    jump();
                    echo("mkdir [name]");
                    echo("  Create a folder named [name] in the current group");
                    jump();
                    echo("mkser [name]");
                    echo("  Create a service named [name] in the current group");
                    jump();
                    echo("mkdoc [name]");
                    echo("  Create a document named [name] in the current group");
                    jump();
                    echo("createRule [ruleName] [fromCategory] [toCategory]");
                    echo("  Create rule [ruleName], which is applicable from [fromCategory] objects to [toCategory] objects");
                    jump();
                    echo("setCat [category] [objectUri]");
                    echo("  Define the category [category] for the [objectUri] targeted object in the current group");
                    jump();
                    echo("applyRel [relationName] [objectUri1] [objectUri2]");
                    echo("  Apply the predefined relation [relationName] from object [objectUri1] to [objectUri2]");
                    jump();
                    echo("listRel");
                    echo("  List the relations for the current folder");
                    jump();
                    echo("listRel [uri]");
                    echo("  List the relations for the object targeted by [uri]");
                    jump();
                    echo("disconnect");
                    echo("  Disconnect current user ");
                    jump();
                    echo("exit");
                    echo("  ByeBye :-) ");
                    jump();
            }
        } catch (Exception e) {
            invalidCommand(e);
        }
    }

    private static void echo(Object object) {
        System.out.println(object);
    }

    private static void err(Object object) {
        System.err.println(object);
    }

    private static void jump() {
        System.out.println();
    }

    private static String scan() {
        return scanner.nextLine();
    }

    private static void invalidCommand(Exception e) {
        err("Invalid command.");
        err("Cause : " + e.getMessage());
    }
    
    private static void refreshContext() {
        if(currentUser == null) {
            context = ">";
        } else {
            context = currentUser.getCurrentGroup() + "@" + currentUser.toString() + ">";
        }
    }

}
