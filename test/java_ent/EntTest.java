/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java_ent.NotSuperUserException;
/**
 *
 * @author Hugo
 */
public class EntTest {
    
    private Ent ent;
    
    public EntTest() {
        this.ent = Ent.Instance();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Ent.Reset();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Ent.
     */
    @Test
    public void testMain() {
    }
    
    @Test(expected=NotSuperUserException.class)
    public void testUserMustBeSuperUserToAddRelationRules() throws NotSuperUserException{
        User noSuperUser = new User("Gilbert");
        this.ent.addRule(noSuperUser,"Relation test","from","to");
    }
    
    @Test
    public void testSuperUserCanAddRules() throws NotSuperUserException {
        User superUser = new User("Super gilbert");
        this.ent.addSuperUser(superUser);
        this.ent.addRule(superUser,"Relation test","from","to");
    }
}