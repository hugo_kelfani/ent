/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import java.util.ArrayList;

/**
 *
 * @author Hugo
 */
public class User {
    private ArrayList<Group> groups;
    private String name;
    private Ent ent;
    
    //Displaying
    private Group currentGroup = null;
    
    public User(String name) {
        this.groups = new ArrayList();
        this.name = name;
        this.ent = Ent.Instance();
    }
    
    public Group getGroupByName(String name) {
        for(Group g:this.groups) {
            if(g.isNamed(name)) {
                return g;
            }
        }
        return null;
    }
    
    /**
     * Create a new group and define the user as group manager
     * @param name group name
     * @throws Exception 
     */
    public void createGroup(String name) throws Exception {
        if(this.groups.contains(this.getGroupByName(name))){
            throw new Exception("Already a group named "+name+" for user "+this.name);
        }
        Group g = new Group(name, this);
        this.groups.add(g);
        this.ent.addGroup(g);
    }
    
    /**
     * Create a new group and define the user as group manager
     * @param name group name
     * @throws Exception 
     */
    public void createGroup(String name, String targetedGroup) throws Exception {
        if(this.groups.contains(this.getGroupByName(name))){
            throw new Exception("Already a group named "+name+" for user "+this.name);
        }
        
        Group template = this.getGroupByName(targetedGroup);
        if(template == null) {
            throw new Exception("Targeted template group doesn't exist");
        }
        
        Group g = new Group(name, this, template);
        this.groups.add(g);
        this.ent.addGroup(g);
    }
    
    /**
     * Add a new group to visible groups
     * @param name
     * @throws Exception 
     */
    public void addGroup(String name) throws Exception {
        if(this.groups.contains(this.getGroupByName(name))){
            throw new Exception("Already a group named "+name+" for user "+this.name);
        }
        Group g = this.ent.getGroupByName(name);
        this.groups.add(g);
    }
    
    /**
     * 
     * @param name
     * @return 
     */
    public boolean isNamed(String name) {
        return this.name.equals(name);
    }

    /**
     * If manager of group, add a user to the group
     * @param user
     * @param group
     * @throws Exception 
     */
    public void addUserInGroup(String user, String group) throws Exception {
        if(this.ent.getGroupByName(group).checkManager(this)) {
            this.getUserByName(user).addGroup(group);
        }
        else {
            throw new Exception("User "+this.name+" can not add user in group "+group+" because he is not the manager of the group");
        }
    }

    @Override
    public String toString() {
        return this.name;
    }

    public String listGroups() {
        String returned = "";
        if(this.groups.isEmpty()) {
            return "No groups found for this user";
        }
        
        for(Group g : this.groups) {
            returned += g.displayName() + ((g.checkManager(this)) ? "*" : "") + "\n"; 
        }
        
        return returned;
        
    }
    
    private User getUserByName(String user) throws Exception {
        return this.ent.getUserByName(user);
    }
    
    public void addRule(String relationName, String from, String to) throws NotSuperUserException {
        try {
            this.ent.addRule(this, relationName, from, to);
        } catch (NotSuperUserException e) {
            throw new NotSuperUserException();
        }
    }
    
    //Displaying
    public void openGroup(String groupName) throws Exception {
        this.currentGroup = this.getGroupByName(groupName);
        if(this.currentGroup == null) {
            throw new Exception("Group not found");
        }
    }
    
    public String getCurrentGroup() {
        if(this.currentGroup == null) {
            return "";
        }
        
        return this.currentGroup.getCurrentFolder();
    }
    
    public void goTo(String uri) throws Exception {
        if(this.currentGroup == null) {
            throw new Exception("A group must be opened");
        }
        try {
            this.currentGroup.goTo(uri);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public String displayContent() throws Exception {
        try {
            return this.currentGroup.displayCurrentFolderContent();
        } catch (Exception e) {
            throw new Exception("A group must be opened");
        }
    }
    
    public void setCategory(String cat, String targetURI) throws Exception {
        if(this.currentGroup == null) {
            throw new Exception("A group must be opened");
        }
        try {
            this.currentGroup.setCategory(cat, targetURI);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void applyRelation(String name, String uriFrom, String uriTo) throws Exception {
        this.currentGroup.applyRelation(name, uriFrom, uriTo);
    }
    
    public String displayRelations() {
        return this.currentGroup.listRelations();
    }
    
    public String displayRelations(String uri) throws Exception {
        return this.currentGroup.listRelations(uri);
    }
    
    public void createDocument(String name) throws Exception {
        this.currentGroup.createDocument(name);
    }
    public void createService(String name) throws Exception {
        this.currentGroup.createService(name);
    }
    public void createFolder(String name) throws Exception {
        this.currentGroup.createFolder(name);
    }
}
