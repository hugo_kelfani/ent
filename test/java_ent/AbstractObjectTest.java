/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class AbstractObjectTest {
    
    private Ent ent;
    
    public AbstractObjectTest() {
        this.ent = Ent.Instance();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Ent.Reset();
        this.ent = Ent.Instance();
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testAddCategoryToAbstractObject() throws NotSuperUserException {
        User superUser = new User("André");
        this.ent.addSuperUser(superUser);
        this.ent.addRule(superUser, "", "from", "to");
        Category cat = new Category("FROM");
        ConcreteObject co = new ConcreteObject(null,null);
        co.setCategory("FROM");
        
        assertEquals(co.getCategory(), new Category("from"));
    }
    
    @Test
    public void testGetAbsoluteURIForFolder() throws Exception {
        Folder folder1 = new Folder("Folder1", null);
        folder1
            .addSubFolderAndGetChild("Folder11")
            .addSubFolderAndGetChild("Folder111")
            .addSubFolderAndGetChild("Folder1111");
        AbstractObject folder1111 = folder1.getByURI("Folder11/Folder111/Folder1111");
        
        assertEquals("Folder1/Folder11/Folder111/Folder1111",folder1111.getAbsoluteURI());
    }
    
    @Test
    public void testGetAbsoluteURIForServiceOrDocument() throws Exception {
        Folder folder1 = new Folder("Folder1", null);
        folder1
            .addSubFolderAndGetChild("Folder11")
            .addSubFolderAndGetChild("Folder111")
            .addDocument("Document");
        AbstractObject doc = folder1.getByURI("Folder11/Folder111/Document");
        
        assertEquals("Folder1/Folder11/Folder111/Document",doc.getAbsoluteURI());
    }

    @Test
    public void testListRelations() throws Exception {
        RelationFactory relationFactory = this.ent.getRelationFactory();
        
        relationFactory.addRule("fromto", "from", "to");
        relationFactory.addRule("tofrom", "to", "from");
        relationFactory.addRule("fromfrom", "from", "from");
        
        Folder root = new Folder("root", null);
        root
            .addSubFolderAndGetChild("sub1")
                .addSubFolderAndChain("sub11")
                .addSubFolderAndChain("sub12");
        
        root
            .goTo("sub1")
                .addDocument("sub1-Doc1")
                .addDocument("sub1-Doc2")
                .goTo("sub11")
                    .addDocument("sub11-Doc1");
        
        root.setCategory("from");
        root.getByURI("sub1").setCategory("to");
        root.getByURI("sub1/sub11").setCategory("from");
        root.getByURI("sub1/sub11/sub11-Doc1").setCategory("from");
        root.getByURI("sub1/sub1-Doc1").setCategory("to");
        root.getByURI("sub1/sub1-Doc2").setCategory("to");
        root.getByURI("sub1/sub12").setCategory("to");
        
        relationFactory.applyRelation("fromto", root, root.getByURI("sub1"));
        relationFactory.applyRelation("fromto", root, root.getByURI("sub1/sub12"));
        relationFactory.applyRelation("fromto", root, root.getByURI("sub1/sub1-Doc1"));
//        relationFactory.applyRelation("fromto", root.getByURI("sub1/sub11"), root.getByURI("sub1"));
//        relationFactory.applyRelation("fromto", root.getByURI("sub1/sub11"), root.getByURI("sub1/sub1-Doc1"));
//        relationFactory.applyRelation("fromto", root.getByURI("sub1/sub11/sub11-Doc1"), root.getByURI("sub1"));
//        relationFactory.applyRelation("fromto", root.getByURI("sub1/sub11/sub11-Doc1"), root.getByURI("sub1/sub1-Doc1"));
        
//        relationFactory.applyRelation("tofrom", root.getByURI("sub1"), root.getByURI("sub1/sub11"));
//        relationFactory.applyRelation("tofrom", root.getByURI("sub1"), root.getByURI("sub1/sub11/sub11-Doc1"));
        relationFactory.applyRelation("tofrom", root.getByURI("sub1/sub1-Doc1"), root);
        relationFactory.applyRelation("tofrom", root.getByURI("sub1/sub1-Doc2"), root);

        relationFactory.applyRelation("fromfrom", root, root);
        relationFactory.applyRelation("fromfrom", root, root.getByURI("sub1/sub11"));
        relationFactory.applyRelation("fromfrom", root, root.getByURI("sub1/sub11/sub11-Doc1"));
//        relationFactory.applyRelation("fromfrom", root.getByURI("sub1/sub11"), root.getByURI("sub1/sub11/sub11-Doc1"));
        
        assertEquals(
            "fromto > root/sub1\n"+
            "fromto > root/sub1/sub12\n"+
            "fromto > root/sub1/sub1-Doc1\n"+
            "fromfrom > root\n"+
            "fromfrom > root/sub1/sub11\n"+
            "fromfrom > root/sub1/sub11/sub11-Doc1\n"+
            "tofrom < root/sub1/sub1-Doc1\n"+
            "tofrom < root/sub1/sub1-Doc2\n"+
            "fromfrom < root\n"
                ,
            root.listRelations()
        );        
    }
    
}

class ConcreteObject extends AbstractObject {

    public ConcreteObject(String name, Folder parent) {
        super(name, parent);
    }

    @Override
    public Boolean isNamed(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean isFolder() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}