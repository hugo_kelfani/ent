/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Hugo
 */
public class UserTest {
    
    public Ent entTest;
    public User user1;
    public String userName1 = "test1";
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void addedGroupExists() throws Exception {
        Ent.Reset();
        User u1 = new User("User1");
        u1.createGroup("Group1");
        assertNotNull(u1.getGroupByName("Group1"));
    }
    
    @Test
    public void addedGroupIsValid() throws Exception {
        Ent.Reset();
        User u1 = new User("User1");
        u1.createGroup("Group1");
        assertTrue(u1.getGroupByName("Group1").isNamed("Group1"));
    }
    
    @Test(expected=Exception.class)
    public void groupNameIsUniqueForAnUser() throws Exception{
        Ent.Reset();
        User u1 = new User("User1");
        u1.createGroup("Group1");
        u1.createGroup("Group1");
    }
    
    
    @Test
    public void groupNameNotIsUniqueForDifferentUsers() throws Exception{
        Ent.Reset();
        User u1 = new User("User1");
        User u2 = new User("User2");
        u1.createGroup("Group1");
        u2.createGroup("Group1");
    }
    
    @Test
    public void creatorOfTheGroupIsItsManager() throws Exception {
        Ent.Reset();
        User u1 = new User("User1");
        u1.createGroup("Group1");
        assertTrue(u1.getGroupByName("Group1").checkManager(u1));
    }
    
    @Test
    public void groupManagerCanAddViewer() throws Exception {
        Ent.Reset();
        User u1 = new User("User1");
        User u2 = new User("User2");
        Ent ent = Ent.Instance();
        ent.addUser(u1);
        ent.addUser(u2);
        u1.createGroup("Group");
        u1.addUserInGroup("User2", "Group");
        assertNotNull(u2.getGroupByName("Group"));
    }
    
    @Test(expected=Exception.class)
    public void userCantAddHimselfInAGroupHeManages() throws Exception{
        Ent.Reset();
        User u1 = new User("User1");
        u1.createGroup("Group");
        Ent.Instance().addUser(u1);
        u1.addUserInGroup("User1", "Group");
    }
    
    @Test(expected=Exception.class)
    public void userWhoAddsOtherUsersInAGroupMustBeManagerOfThisOne() throws Exception{
        Ent.Reset();
        User u1 = new User("User1");
        User u2 = new User("User2");
        User u3 = new User("User3");
        u1.createGroup("Group");
        Ent.Instance().addUser(u1);
        Ent.Instance().addUser(u2);
        Ent.Instance().addUser(u3);
        u2.addUserInGroup("User3", "Group");
    }
}