/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Hugo
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({UserTest.class, ServiceTest.class, DocumentTest.class, EntTest.class, FolderTest.class, AbstractObjectTest.class, GroupTest.class})
public class Java_entSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}