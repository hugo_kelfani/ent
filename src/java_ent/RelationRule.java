/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import java.util.ArrayList;

/**
 *
 * @author Yohann
 */
class RelationRule {
    private String name;
    private Category from;
    private Category to;
    
    public RelationRule(String name, Category fromCat, Category toCat) {
        this.from = fromCat;
        this.to = toCat;
        this.name = name;
    }
    
    public boolean isAppliable(String name, Category from, Category to) {
        return 
                this.from.equals(from) && 
                this.to.equals(to) &&
                this.name.toLowerCase().equals(name.toLowerCase());
    }
    
    @Override
    public boolean equals(Object o) {
        if(!(o instanceof RelationRule)) {
            return false;
        }
        RelationRule rr = (RelationRule) o;
        return rr.name.toLowerCase().equals(this.name.toLowerCase());
    }
    
    @Override
    public String toString() {
        return this.name + " : " + this.from.toString() + " > " + this.to.toString();
    }
}
