/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yohann
 */
public class RelationFactoryTest {
    
    private Ent ent = Ent.Instance();
    
    public RelationFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        Ent.Reset();
        this.ent = Ent.Instance();
    }
    
    @After
    public void tearDown() {
    }

//    @Test
//    public void testSomeMethod() {
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
    @Test
    public void testAddRuleAddsCategories() {
        RelationFactory rf = new RelationFactory();
        rf.addRule("RuleName","FromCategory","ToCategory");
        assertEquals(rf.countCategories(),2); 
    }
    
    @Test
    public void testCategoriesCantContainsTheSameCategoryTwice() {
        RelationFactory rf = new RelationFactory();
        rf.addRule("RuleName","FromCategory","ToCategory");
        rf.addRule("RuleName2","FromCategory2","Tocategory");
        assertEquals(3,rf.countCategories()); 
    }
    
    @Test
    public void testAddRules() {
        RelationFactory rf = new RelationFactory();
        rf.addRule("RuleName","FromCategory","ToCategory");
        rf.addRule("RuleName2","FromCategory2","Tocategory");
        assertEquals(2,rf.countRules()); 
    }
    
    @Test
    public void testAddRulesCantAddTheSameNamedRuleTwice() {
        RelationFactory rf = new RelationFactory();
        rf.addRule("RuleName","FromCategory","ToCategory");
        rf.addRule("Rulename","FromCategory2","Tocategory");
        assertEquals(1,rf.countRules()); 
    }
    
    @Test
    public void testCreateRelationBetweenTwoCategorizedObjects() throws Exception {
        RelationFactory rf = Ent.Instance().getRelationFactory();
        rf.addRule("fromto", "from", "to");
        ConcreteObject co1 = new ConcreteObject(null,null);
        ConcreteObject co2 = new ConcreteObject(null,null);
        co1.setCategory("from");
        co2.setCategory("to");
        
        rf.applyRelation("fromto",co1,co2);
        assertEquals(1,co1.countOutgoingRelations());
        assertEquals(0,co2.countOutgoingRelations());
        assertEquals(1,co2.countIncomingRelations());
        assertEquals(0,co1.countIncomingRelations());
    }
    
    @Test(expected = Exception.class)
    public void testCheckCategoriesWhenCreateRelation() throws Exception {
        RelationFactory rf = Ent.Instance().getRelationFactory();
        rf.addRule("fromto", "from", "to");
        rf.addRule("fail", "fail", "fail");
        ConcreteObject co1 = new ConcreteObject(null,null);
        ConcreteObject co2 = new ConcreteObject(null,null);
        co1.setCategory("from");
        co2.setCategory("fail");
        
        rf.applyRelation("fromto",co1,co2);
    }
    
    @Test
    public void testIncomingAndOutgoingRelations() throws NotSuperUserException, Exception {
        User superUser = new User("André");
        this.ent.addSuperUser(superUser);
        this.ent.addRule(superUser, "fromto", "from", "to");
        this.ent.addRule(superUser, "handle", "to", "from");
        this.ent.addRule(superUser, "is", "to", "to");
        ConcreteObject co1 = new ConcreteObject(null,null);
        co1.setCategory("FROM");
        ConcreteObject co2 = new ConcreteObject(null,null);
        co2.setCategory("TO");
        RelationFactory relationFactory = this.ent.getRelationFactory();
        
        relationFactory.applyRelation("fromto", co1, co2);
        relationFactory.applyRelation("handle", co2, co1);
        relationFactory.applyRelation("is", co2, co2);
        
        assertEquals(co1.countIncomingRelations(), 1);
        assertEquals(co2.countIncomingRelations(), 2);
        assertEquals(co1.countOutgoingRelations(), 1);
        assertEquals(co2.countOutgoingRelations(), 2);
    }
    
    @Test(expected = Exception.class)
    public void testCheckRelationValidity() throws Exception {
        Ent.Instance().getRelationFactory().addRule("fromto", "from", "to");
        ConcreteObject co1 = new ConcreteObject(null,null);
        ConcreteObject co2 = new ConcreteObject(null,null);
        co1.setCategory("from");
        
        Ent.Instance().getRelationFactory().applyRelation("fromto",co1,co2);
    }
    
}