/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Yohann
 */
class RelationFactory {
    private ArrayList<RelationRule> rules;
    private ArrayList<Category> categories;

    public RelationFactory() {
        this.rules = new ArrayList<RelationRule>();
        this.categories = new ArrayList();
    }
    public void addRule(String ruleName, String fromCategory, String toCategory) {
        Category fromCat = new Category(fromCategory);
        Category toCat = new Category(toCategory);
        if(!this.categories.contains(fromCat)) {
            this.categories.add(fromCat);
        }
        if(!this.categories.contains(toCat)) {
            this.categories.add(toCat);
        }
        
        RelationRule rr = new RelationRule(ruleName, this.getCategoryByName(fromCategory), this.getCategoryByName(toCategory));
        if(!this.rules.contains(rr)) {
            this.rules.add(rr);
        }
        
    }

    public int countCategories() {
        return this.categories.size();
    }
    
    public int countRules() {
        return this.rules.size();
    }
    
    public Category getCategoryByName(String name) {
        for(Category c : this.categories) {
            if(c.equals(new Category(name))) {
                return c;
            }
        }
        return null;
    }

    public void applyRelation(String name, AbstractObject from, AbstractObject to) throws Exception {
        if(from.getCategory() == null || to.getCategory() == null) {
            throw new Exception("An object has no category.");
        }
        
        for(RelationRule rule : this.rules) {
            if(rule.isAppliable(name,from.getCategory(), to.getCategory())) {
                Relation r = new Relation(name, from, to);
                from.addOutgoingRelation(r);
                to.addIncomingRelation(r);
                return;
            }
        }
        
        throw new Exception("No rule appliable");
        
    }
    
    public String listRules() {
        if(this.rules.isEmpty()) {
            return "Actually no rules";
        }
        
        String returned = "";
        for(RelationRule rule : this.rules) {
            returned += rule.toString() + "\n";
        }
        
        return returned;
    }
    
}
