/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_ent;

import java.util.ArrayList;

/**
 *
 * @author Hugo
 */
public class Ent {
    private ArrayList<User> users;
    private ArrayList<Group> groups;
    private static Ent Instance;
    private ArrayList<User> superUsers;
    private RelationFactory relationFactory;
    
    private Ent() {
        this.users = new ArrayList<User>();
        this.groups = new ArrayList<Group>();
        this.superUsers = new ArrayList<User>();
        this.relationFactory = new RelationFactory();
    }

    // /!\ ONLY FOR TESTS
    public static void Reset() {
        Ent.Instance = new Ent();
    }
    
    public static Ent Instance() {
        if(Ent.Instance == null) {
            System.out.println("");
            Ent.Instance = new Ent();
        }
        return Ent.Instance;
    }
    
    public void addUser(User user) {
        this.users.add(user);
    }

    public User getUserByName(String user) throws Exception {
       for(User u : this.users) {
           if(u.isNamed(user)) {
               return u;
           }
       }
       throw new Exception("No user named "+user);
    }

    public Group getGroupByName(String name) throws Exception {
      for(Group g : this.groups) {
           if(g.isNamed(name)) {
               return g;
           }
       }
       throw new Exception("No group named "+name);
    }

    public void addGroup(Group g) {
        this.groups.add(g);
    }

    public void addRule(User user, String relationName, String from, String to) throws NotSuperUserException {
        if(!this.superUsers.contains(user)){
            throw new NotSuperUserException();
        }
        this.relationFactory.addRule(relationName, from, to);
    }

    public void addSuperUser(User superUser) {
        this.superUsers.add(superUser);
    }

    public Category getCategoryByName(String name) {
        return this.relationFactory.getCategoryByName(name);
    }

    public int getRelationFactoryCategoriesSize() {
        return this.relationFactory.countCategories();
    }

    public RelationFactory getRelationFactory() {
        return this.relationFactory;
    }
    
    public String listUsers() {
        if(this.users.size() == 0) {
            return "Actually no users.";
        }
        
        String returned = "";
        for(User user : this.users) {
            returned += user.toString() +((checkSuperUser(user)) ? "*" : "")+ "\n";
        }
        
        return returned;
    }
    
    public String listRules() {
        return this.relationFactory.listRules();
    }
    
    public boolean checkSuperUser(User u) {
        return this.superUsers.contains(u);
    }
}
